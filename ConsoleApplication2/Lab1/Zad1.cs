﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Lab1
{
    class Zad1
    {
        public static int AverageCreationTime, AverageShutdownTime = 0;
        public static int SumCreationTimes, SumShutdownTimes = 0;
        public static Stopwatch CreationTime = new Stopwatch();
        public static Stopwatch ShutdownTime = new Stopwatch();
        public static void timeBurner()
        {
                for (int i = 0; i < 10; i++)
                {
                    
                }   
        }

        public static void summarize()
        {
            for (int i = 0; i < 1000; i++)
            {
                Thread Measures = new Thread(Zad1.timeBurner);
                CreationTime.Start();
                Measures.Start();
                CreationTime.Stop();

                ShutdownTime.Start();
                Measures.Abort();
                ShutdownTime.Stop();

                SumCreationTimes += Convert.ToInt32(CreationTime.ElapsedMilliseconds);
                SumShutdownTimes += Convert.ToInt32(ShutdownTime.ElapsedMilliseconds);
            }
            AverageCreationTime = SumCreationTimes / 1000;
            AverageShutdownTime = SumShutdownTimes / 1000;
            Console.WriteLine("Average Creation:");
            Console.WriteLine(AverageCreationTime);
            Console.WriteLine("Average Shutdown:");          
            Console.WriteLine(AverageShutdownTime);
        }
        
    }
}
