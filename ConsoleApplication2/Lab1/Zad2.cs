﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab1
{
    class Zad2
    {
        public static int counter;
        public static void literallyNothing()
        {
            do
            {
                int beingDeclaredForFun;
            } while (true);
        }

        public static void goodbyeRAM()
        {
            try
            {
                while (true)
                {
                    Thread oneOfMany = new Thread(Zad2.literallyNothing);
                    oneOfMany.Start();
                    Console.WriteLine("Currently at " + Zad2.counter + " Threads");
                    Zad2.counter++;
                }
            }
            catch (OutOfMemoryException)
            {
                Console.WriteLine("Died at" +Zad2.counter +"Threads");
            }
        }
    }
}
