﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab1
{
    class Zad3
    {
    static int[,] graph;
        static int predefinedN=500;
        public static int count;
        public static void create()
        {
            graph=new int[predefinedN,predefinedN];
            Random Number = new Random();
            for (int i = 0; i < predefinedN; i++)
            {
                for (int j = i + 1; j < predefinedN; j++)
                {
                    graph[i, j] =  Number.Next(0, 2);
                    graph[j, i] = graph[i, j];
                }
                graph[i, i] = 0;
            }
            showGraph();
        }

        public static void showGraph()
        {
            foreach (int graphValue in graph)
            {
                Console.Write(graphValue);
            }
        }

        public static void sequentialCount()
        {
             count = 0;
             for (int i = 0; i < predefinedN - 1; i++)
             {
                 for (int j = i + 1; j < predefinedN; j++)
                 {
                     if (graph[i, j] == 1)
                         count++;
                 }
             }
            
        Console.WriteLine(" "+count+" found");
            count = 0;
        }

        public static void threadedCount()
        {
            for (int i = 0; i < predefinedN; i++)
            {
                count += graph[i,
                    int.Parse(Thread.CurrentThread.Name)];
                

            }
            
        }

        public static void Measure()
        {
            Stopwatch timer = new Stopwatch();
            create();
            timer.Start();
            sequentialCount();
            timer.Stop();
            Console.WriteLine("Sequential: " +timer.ElapsedMilliseconds + "ms");
            timer.Reset();

            timer.Start();
            Thread[] table = new Thread[predefinedN];
            for (int i = 0; i < predefinedN; i++)
            {
                table[i] = new Thread(new ThreadStart(threadedCount));
                table[i].Name = Convert.ToString(i);
            }
            for (int i = 0; i < predefinedN; i++)
            {
                 table[i].Start();
                table[i].Join();
            }
            timer.Stop();
            Console.WriteLine("Threaded: "+timer.ElapsedMilliseconds+" ms");
            Console.WriteLine(count/2 + " found");
        }
    }
}
