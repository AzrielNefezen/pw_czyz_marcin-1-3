﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab2
{
    class Zad2
    {
        
        public static int[] table;
        

        public static void init()
        {

            table = new int[] { 1, 2, 3, 4, 5, 6 };

        }

        public static void run()
        {
            init();
            Thread sum = new Thread(Zad2.sum);
            sum.Start();
            
        }

        public static void sum()
        {
            int result1 = 0;
            Parallel.For(0, table.Length,
                () => 0, (j, loop, lResult) =>
                {
                    lResult += table[j];
                    return lResult;

                },
                (x) => { Thread.BeginCriticalRegion();
                        Interlocked.Add(ref result1, x);
                        Thread.EndCriticalRegion();
        }

    );
        }

    }
}
