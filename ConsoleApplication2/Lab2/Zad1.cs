﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab2
{
    class Zad1
    {
        public static int result = 0;
        public static int[] table;
        public static int currIndex = 0;
        private static Object crit = new Object();
        
        public static void sum()
        {
            
            Parallel.For(0, table.Length,
                () => 0, (j, loop, lResult) =>
                {
                        lResult += table[j];
                    
                    return lResult;  
                },
                (x) =>
                {
                    lock (crit)
                    {
                        result += x;
                    }
                });

        }

        public static void init()
        {

            table = new int[] { 1, 2, 3, 4, 5, 6 };

        }

        public static void run()
        {
            init();
            Thread sum = new Thread(Zad1.sum);
            sum.Start();
        }
    }
}
