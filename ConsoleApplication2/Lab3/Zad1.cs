﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab3
{
    class Zad1
    {
        public static string Pass;
        public static string _Pass;

        static List<String> buffer = new List<string>();

        static Thread Producer;
        static Thread Consumer;
        static Thread bFThread;

        static void generatePass(int _length)
        {
            Random _character = new Random();
            char[] _table = new char[_length];

            for (int i = 0; i < _length; i++)
            {
                _table[i] = (char)_character.Next(97, 122);
            }

            Pass = new String(_table);

            Console.WriteLine(Pass);
        }


        public static void bFPass()
        {
            buffer.Add("a");

            Producer = new Thread(new ThreadStart(Produce));
            Consumer = new Thread(new ThreadStart(Consume));

            Producer.Start();
            Consumer.Start();

            while (true)
            {
                if (!Producer.IsAlive)
                {
                    Consumer.Abort();
                    Producer.Abort();
                    break;
                }
            }
        }

        public static void Produce()
        {
            char[] _tab = new char[Pass.Length];
            Random _character = new Random();
            while (true)
            {
                for (int j = 0; j < Pass.Length; j++)
                {
                    _tab[j] = (char)_character.Next(97, 122);
                }
                _Pass = new String(_tab);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("<Producer> Generated Pass " + _Pass);
                buffer.Add(_Pass);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("<BUFFER> Pass added. Current reserved capacity: " + buffer.Capacity);

                Thread.Sleep(1);

            }
        }

        public static void Consume()
        {
            while (true)
            {

                if (buffer.First() != Pass)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<Consumer> Wrong Password - " + buffer.First());
                    buffer.Remove(buffer.First());
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("<Consumer> Password Found - " + buffer.First());
                    Console.ResetColor();
                    Producer.Abort();

                }
                Thread.Sleep(2);

            }
        }

        public static void run()
        {
            generatePass(3); 

            bFThread = new Thread(new ThreadStart(bFPass));
            bFThread.Start();

            Console.ReadLine();
        }
    }
}
