﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6_LR
{
    public partial class Form1 : Form
    {
        private int[,] table;
        private int height, width, height1, width1;
        private Bitmap image, image2;
        private int[,] table1;

        public Form1()
        {
            InitializeComponent();
            button2.Enabled = false;
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Image File|*.png";

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                image = new Bitmap(openFileDialog1.OpenFile());
                button2.Enabled = true;
                width = image.Width;
                int a;
                height = image.Height;
                table = new int[width, height];
                for (int i = 0; i < width; i++)
                    for (int j = 0; j < height; j++)
                    {
                        a = image.GetPixel(i, j).B;
                        if (a == 255)
                            table[i, j] = 0;
                        if ((a < 255) & (a > 50))
                            table[i, j] = 0;
                        if (a < 49)
                            table[i, j] = 1;
                    }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (table[j, i] == 1)
                    {
                        richTextBox1.SelectionColor = System.Drawing.Color.Red;
                        richTextBox1.AppendText(" " + table[j, i].ToString());
                    }
                    else
                    {
                        richTextBox1.SelectionColor = System.Drawing.Color.Black;
                        richTextBox1.AppendText(table[j, i].ToString());
                    }
                }
                richTextBox1.AppendText("\n");
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Image File|*.png";

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                image2 = new Bitmap(openFileDialog1.OpenFile());
                width1 = image2.Width;
                int a;
                height1 = image2.Height;
                table1 = new int[width1, height1];
                for (int i = 0; i < width1; i++)
                    for (int j = 0; j < height; j++)
                    {

                        a = image2.GetPixel(i, j).B;
                        if (a == 255)
                            table1[i, j] = 0;
                        if (a < 49)
                            table1[i, j] = 1;
                    }
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            int sum = 0;
            for (int i = 0; i < width1; i++)
                for (int j = 0; j < height; j++)
                {
                    sum += table[i, j] * table1[i, j];
                }
            label1.Text = sum.ToString();
        }
    }
}