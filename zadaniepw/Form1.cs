﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;


namespace zadaniePW
{
    public partial class Form1 : Form
    {
        int i = 1;
        private static int factValue = 0;
        private static int temp = 0;
        Int64 ETA = 0;

        static Stopwatch timer = new Stopwatch();
      
        static Thread counterThread;

        private static int factorial(int i)
        {
           return(i < 1? 1: i * factorial(i - 1));
        }

        public void threadedfact()
        {
            timer.Start();
            factValue = factorial(Convert.ToInt16(textBox1.Text));
            label7.Text = Convert.ToString(factValue);
            progressBar1.Maximum = factValue;
            progressBar1.Minimum = 0;

            counterThread = new Thread(new ThreadStart(run));
            counterThread.Start();
            if(!counterThread.IsAlive)
                timer.Stop();
        }

        public void sequentialfact()
        {
            timer.Start();
            factValue = factorial(Convert.ToInt16(textBox1.Text));
            label7.Text = Convert.ToString(factValue);
            progressBar1.Maximum = factValue;
            progressBar1.Minimum = 0;



            for (i = 1; i < factValue + 1; i++)
            {
                progressBar1.Value = i;


                if (i % 10 == 0)
                {
                    ETA = ((factValue - i) * Convert.ToInt64(timer.ElapsedMilliseconds) / i);
                    label6.Text = Convert.ToString(ETA / 1000) + " s";
                    label5.Text = Convert.ToString(timer.ElapsedMilliseconds / 1000) + " s";
                    label5.Refresh();
                    label6.Refresh();

                }


            }


            timer.Stop();
            label5.Text = Convert.ToString(timer.ElapsedMilliseconds / 1000) + " s";
        }

        public void run()
        {
            while (true)
            {
                long time = timer.ElapsedMilliseconds;
                MethodInvoker bar = 
                    () => 
                        progressBar1.Value = temp;
                MethodInvoker Estim = 
                    () =>
                {
                    label6.Text = Convert.ToString(ETA/1000) + " s";
                    label6.Refresh();
                };
                MethodInvoker Eta =
                    () =>
                    {
                        label5.Text = Convert.ToString(time/1000) + " s";
                        label5.Refresh();
                    };
            progressBar1.Invoke(bar);
                label5.Invoke(Eta);
                label6.Invoke(Estim);
                ETA = ((factValue - i) * Convert.ToInt64(time) / i);


            temp++;
                i++;
                time++;

                if (factValue == temp)
                {
                    counterThread.Abort();
                    break;
                }                
            }
        }



        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text!="")
            sequentialfact();
            else
            {
                MessageBox.Show("Z pustego i Salomon nie naleje...");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {   
                threadedfact();
            }
            else
            {
                MessageBox.Show("Z pustego i Salomon nie naleje...");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (counterThread.IsAlive)
            {
                timer.Stop();
                counterThread.Suspend();
            }
            else
            {
                timer.Start();
                counterThread.Resume();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            counterThread.Abort();
            progressBar1.Value = 0;
            i = 1;
            factValue = 0;
            temp = 0;
            ETA = 0;
            timer.Reset();
            label6.Text = "0";
            label5.Text = "0";
        }

        
    }
}
